const {Transform} = require('stream')

class SSE extends Transform{
	constructor(ctx, alive){
		super({writableObjectMode: true})
		//XXX do i need these? didn't seem to cause any problems so far
		// ctx.req.socket.setTimeout(alive) //this was 0 by default, and in the docs also 0
		// ctx.req.socket.setNoDelay(true)
		// ctx.req.socket.setKeepAlive(true)
		ctx.set({
			'Content-Type': 'text/event-stream',
			'Cache-Control': 'no-cache, no-transform', //no buffering
			'X-Accel-Buffering': 'no', //no buffering
			'Connection': 'keep-alive',
			'Keep-Alive': `timeout=${alive/1000 |0}` //client will keep alive at minimum this, in seconds
		})
	}
	_transform(data={}, encoding, cb){
		const tokens = []

		if(typeof data === 'string') data = {data}
		for(const [key, value] of Object.entries(data)){
			const stringified = typeof value === 'string'? value:
				typeof value === 'undefined'? '':
				value instanceof Error? value.toString():
				JSON.stringify(value)
			const lines = stringified.split(/(\r\n|\r|\n)/)
			for(const line of lines) tokens.push(`${key}: ${line}`)
		}

		const text = tokens.join('\n')+'\n\n' //extra newlines are necessary for some reason, or it won't send buffer
		this.push(text)
		cb(undefined, text)
	}
	//data can be empty, string (will be turned to nameless event data object), or object (accepts event, entry, id, data fields)
	//events of name close destroy the connection
	send(data, cb){
		this.write(data, encodeURI, ()=>{
			cb()
			if(data && data.event === 'close') this.destroy() //if close sent to client, triggers close event on server that will clean up pool
		})
	}
	//PLEASE close, otherwise leaks
	sendClose(data, cb){this.send({event: 'close', data}, cb)}
	//maybe some kind of weak map would work ? but w/e and idk that stuff.. and i need onRemoval for destroy
}

module.exports = ({
	ping=60000
}={})=>{
	const pool = []
	let keepAlive = false

	const live = ()=> keepAlive = setInterval(()=>{
		if(!pool.length){ //stop auto ping if nothing in pool
			clearInterval(keepAlive)
			keepAlive = false
		}else pool.forEach(sse=> sse.send()) //pings to keep pools active
	}, ping)
	const create = (ctx)=>{
		const sse = new SSE(ctx, ping)
		pool.push(sse)
		sse.on('close', ()=> pool.splice(pool.indexOf(sse), 1))
		if(!keepAlive) live() //if not refreshing liveliness, trigger it
		return sse
	}

	return async (ctx, next)=>{
		let sse
		Object.defineProperty(ctx, 'sse', {get: ()=> sse || (sse = create(ctx))})
		await next()
		if(sse) //only overwrite body if used sse
			ctx.body = ctx.sse //pipes stream. Should call close if client calls close, so say koajs stream docs
	}
}
